<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['id', 'title', 'content', 'user_id', 'date'];

    function user()
    {
        return $this->belongsTo(User::class);
    }

    function comments()
    {
        return $this->belongsToMany(Comment::class);
    }
}
