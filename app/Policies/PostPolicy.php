<?php

namespace App\Policies;

use App\User;
use App\post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;
    

    public function viewAny(User $user)
    {
        //
    }

    public function view(User $user, post $post)
    {
        if($user->id == 1){
            return true;
        }
        else
        {
            return $user->id == $post->user_id;
        }
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, post $post)
    {
        if($user->id == 1){
            return true;
        }
        else
        {
            return $user->id == $post->user_id;
        }
    }

    public function delete(User $user, post $post)
    {
        //
    }

    public function restore(User $user, post $post)
    {
        //
    }

    public function forceDelete(User $user, post $post)
    {
        //
    }

    public function manage(User $user)
    {
        return $user->id == 1;
    }
}
