<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Comment;
use Session;
class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::all();
        $user = \Auth::user();
        if($user->can('manage', Post::class)){
            $posts = Post::all();
        } else{
            $Posts = Post::where('user_id',$user->id)->get();
        }
        return view('post.index', ['posts'=>$posts]);
    }

    public function create()
    {
        $comments=Comment::all();
        $user=User::all();
        return view('post.create',['comments'=>$comments,'user'=>$user]);
    }

    public function storeComment(Request $request)
    {
        $rules=[
            'text' => 'required|max:500',
        ];

        $request->validate($rules);

        $comment = new Comment;
        $comment->fill($request->all());
        $comment->user_id = \Auth::user()->id; 
        $comment->save();

        return redirect('/posts');
    }

    public function show($id)
    {
        $post=Post::find($id);
        $comments=Comment::all();
        $this->authorize('view', $post);
        return view('post.show', ['post'=>$post, 'comments'=>$comments]);
    }

    public function edit($id)
    {
        $post= Post::find($id);
        $this->authorize('update', $post);
        return view('post.edit',['post'=>$post]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'text' => 'required|max:500' . $id,
        ];

        $request->validate($rules);

        $post = Post::findOrFail($id);
        $post->fill($request->all());
        $post->user_id = \Auth::user()->id; 
        $post->save();

        return redirect('/posts/$id');
    }

    public function destroy($id)
    {
        //
    }

    public function like($id,Request $request)
    {
        $post=Post::find($id);
        $request->session()->put('post',$post);
        return back();
     }
}
