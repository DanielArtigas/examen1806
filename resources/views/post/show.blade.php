@extends('layouts.app')

@section('title', 'Posts')

@section('content')
            <h1>
               Post nº.<?php echo $post->id ?>
            </h1>
            @can('view', $post)
            <a  href="/posts/{id}/edit" class="btn btn-primary"  role="button">Editar</a>
            @endcan
            <table>
              <tr>
                  <td>{{$post->content}} </td>
              </tr>
                  <td>Publicado por {{ $post->user->name }} - {{ $post->date }}</td>
            </table> 
            <br>
            <h3>
              Comentarios
            </h3>
            <table>
              <tr>
                <td> {{ $post->user->name }}:</td>
              </tr>
              <td>
              <form action="/posts/{{ $post->id }}" method="post">
                <label>Nuevo comentario</label>
                <input type="text">
                <a  href="/posts/{id}/store" class="btn btn-primary"  role="button">Enviar consulta</a>
              </form>
              
            </td>
            </table>

@endsection