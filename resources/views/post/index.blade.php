@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <table>
        @foreach($posts as $post)
          <tr>
            <th>
              {{ $post->title }}
              @if(Session::has('post'))

                Me gusta {{Session::get('post')->id}}

              @endif
            </th>
          </tr>
          <tr>
            <td>
              {{ $post->content }}<a  href="/posts/{{ $post->id }}">leer mas</a>
            </td>
          </tr> 
            <td>
              {{ $post->user->name }}
            </td> 
            <td>
              {{ $post->date }}
            </td>
            <td><a href="/posts/{id}/like" class="btn btn-primary"  role="button">favorito</a></td>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection